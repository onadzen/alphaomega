<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
	<div id="maincontent" class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell">
				<?php	get_template_part( 'template-parts/page/content', 'page' ); ?>
			</div>
		</div>	
	</div><!-- .maincontent -->
<?php get_footer();
