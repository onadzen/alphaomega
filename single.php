<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
	<div id="maincontent" class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell medium-9 large-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/post/content', get_post_format() );
					endwhile; // End of the loop.
				?>
			</div>
			<div class="cell medium-3 large-3">
				<?php if ( is_active_sidebar( 'widget-sidebar' ) ) : ?>
					<div class="widget-sidebar">
						<?php dynamic_sidebar( 'widget-sidebar' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>	
	</div><!-- .maincontent -->
<?php get_footer();