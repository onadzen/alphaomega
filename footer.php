<?php
/**
 * The template for displaying the footer
 */
?>
			<?php if ( is_active_sidebar( 'top-footer-widget' ) ) : ?>
			<div class="grid-container">
				<div class="grid-x grid-margin-x topfooter">
					<div class="cell top-footer-widget">
						<?php dynamic_sidebar( 'top-footer-widget' ); ?>
					</div>
				</div>	
			</div>	
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer-widget-first' ) || is_active_sidebar( 'footer-widget-second' ) || is_active_sidebar( 'footer-widget-third' ) || is_active_sidebar( 'footer-widget-fourth' ) ) : ?>
			<section class="footer-widgets">
				<div class="grid-container">
					<div class="grid-x grid-margin-x">
						<?php if ( is_active_sidebar( 'footer-widget-first' ) ) : ?>
						<div class="cell medium-3 large-3 footer-widget-first">
							<?php dynamic_sidebar( 'footer-widget-first' ); ?>
						</div>
						<?php endif; ?>
						<?php if ( is_active_sidebar( 'footer-widget-second' ) ) : ?>
						<div class="cell medium-3 large-3 footer-widget-second">
							<?php dynamic_sidebar( 'footer-widget-second' ); ?>
						</div>
						<?php endif; ?>
						<?php if (   is_active_sidebar( 'footer-widget-third' ) ) : ?>
						<div class="cell medium-3 large-3 footer-widget-third">
							<?php dynamic_sidebar( 'footer-widget-third' ); ?>
						</div>
						<?php endif; ?>
						<?php if (   is_active_sidebar( 'footer-widget-fourth' ) ) : ?>
						<div class="cell medium-3 large-3 footer-widget-fourth">
							<?php dynamic_sidebar( 'footer-widget-fourth' ); ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</section>
			<?php endif; ?>
			<footer>
				<div class="grid-container">
					<div class="grid-x grid-margin-x">
						<div class="cell medium-8 large-8 copyright">
							<p style="text-align:center;">&copy; <?php bloginfo('name');?> <?php echo date('Y'); ?> . All Rights Reserved. Website Design and Online Marketing <br/>provided by -  <a href="https://ironballmarketing.com/" target="_blank">Ironball Marketing</a></p>
						</div>
						<div class="cell medium-4 large-4 copyright">
							<p style="text-align:center;"><a href="<?php bloginfo('home')?>/privacy-policy" title="Privacy Policy">Privacy</a> | <a href="<?php bloginfo('home')?>/terms-of-service/" title="Terms & Conditions">Terms & Conditions</a></p>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
<script defer src="<?php bloginfo('template_directory')?>/bower_components/what-input/dist/what-input.js"></script>
<script defer src="<?php bloginfo('template_directory')?>/bower_components/foundation-sites/dist/js/foundation.min.js"></script>
<script defer src="<?php bloginfo('template_directory')?>/js/app.js"></script>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4252767.js"></script>
<!-- End of HubSpot Embed Code -->
</body>
</html>
