<?php
/**
 * The main template file
 */

get_header(); ?>
	<div id="maincontent" class="maincontent-post grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell medium-7 large-9">
				<?php
				if ( have_posts() ) :
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						/*
						* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
						get_template_part( 'template-parts/post/excerpt', get_post_format() );
					endwhile;
				else :
					get_template_part( 'template-parts/post/content', 'none' );
				endif; ?>
			</div>
			<div class="cell medium-5 large-3">
				<?php if ( is_active_sidebar( 'widget-sidebar' ) ) : ?>
					<div class="widget-sidebar">
						<?php dynamic_sidebar( 'widget-sidebar' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>	
	</div><!-- .maincontent-post -->
<?php get_footer();