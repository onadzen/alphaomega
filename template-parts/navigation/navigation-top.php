<?php
/**
 * Displays top navigation
 */
?>
 
      <nav class="navwrapper">
        <div class="title-bar">  
          <div class="title-bar-title">
                <?php
                  //if ( function_exists( 'the_custom_logo' ) ) :
                  //the_custom_logo();
                  //endif; 
                  $custom_logo_id = get_theme_mod( 'custom_logo' );
                  $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                  if ( has_custom_logo() ) {
                    echo '<a href='. get_bloginfo( 'url' ) .'><img src="'. esc_url( $logo[0] ) .'"></a>';
                  } else {
                    echo get_bloginfo( 'name' );
                  } ?>
          </div>

          <div class="title-bar-right hide-for-small-only">
            <?php if ( is_active_sidebar( 'top-menu-widget' ) ) : ?>
              <div class="top-menu-widget">
                <?php dynamic_sidebar( 'top-menu-widget' ); ?>
              </div>
            <?php endif; ?>
            <ul class="menu social-media">
              <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
              <li><a href="https://www.facebook.com/komorowski7/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="#" target="_blank"><i class="fab fa-youtube"></i></a></li>
              <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
              <li><a href="https://homestars.com/companies/2806040-alpha-omega-painting" target="_blank"><img src="https://alphaomegapainting.ca/wp-content/uploads/2018/10/homestars.png" width="60" alt="HomeStars" target="_blank"></a></li>
            </ul>
          </div>      
        </div>

        <div class="top-menu">
          <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell">
              <span class="mobile-menu show-for-small-only">  
                <button class="mobile-icon" type="button" data-open="offCanvas"><i class="fas fa-bars fa-lg"></i></button>
              </span>
              <?php wp_nav_menu( array(
                  'theme_location' => 'main-menu',
                  'container' => 'ul',
                  'menu_id'        => 'main-menu',
                  'menu_class'	 => 'medium-horizontal menu show-for-medium',
                  'items_wrap'     => '<ul id="%1$s" class="%2$s" data-responsive-menu="accordion medium-dropdown" style="width: 100%;">%3$s</ul>',
                  //Recommend setting this to false, but if you need a fallback...
                  'fallback_cb'    => 'f6_drill_menu_fallback',
                  'walker'         => new F6_DRILL_MENU_WALKER()
              ) ); ?>
              <div class="hide-for-small-only">
                <?php if ( is_active_sidebar( 'request-quote-widget' ) ) : ?>              
                  <?php dynamic_sidebar( 'request-quote-widget' ); ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
          </div>
        </div>
      </nav> 
    
      <?php if(!is_front_page()) {  
      if(function_exists('bcn_display')) : ?>
      <div class="breadcrumbs">
        <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell">
              <?php bcn_display(); ?>
            </div>
          </div>        
        </div>
      </div>
      <?php endif;
      } else {
        echo "";
      }
      ?>     
         