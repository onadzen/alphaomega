<?php
/**
 * Template part for displaying posts
  */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h1><?php the_title();?></h1>
	<ul class="menu align-left postmeta">
		<li><i class="fas fa-user"></i> <?php the_author();?></li> 
		<li><i class="fas fa-calendar-alt"></i> <?php the_date('j M, Y');?></li> 
		<li><i class="fas fa-folder-open"></i> <?php the_category(', '); ?></li> 
	</ul>
	<?php
		/* translators: %s: Name of current post */
		the_content();
	?>
</article><!-- #post-## -->
