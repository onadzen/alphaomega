<?php
/**
 * Template part for displaying posts
  */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h2><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
	<ul class="menu align-left postmeta">
		<li><i class="fas fa-user"></i> <?php the_author();?></li> 
		<li><i class="fas fa-calendar-alt"></i> <?php the_date('j M, Y');?></li> 
		<li><i class="fas fa-folder-open"></i> <?php the_category(', '); ?></li> 
	</ul>
	<div class="post-article">
		<?php 
		// check if the post has a Post Thumbnail assigned to it.
		if ( has_post_thumbnail() ) { ?>
		<div class="post-img">
			<a href="<?php the_permalink();?>" title="<?php the_title();?>">
				<?php the_post_thumbnail('large'); ?>
			</a>	
		</div>
		<?php } else { 
			echo "";
		} ?>
		<div class="post-excerpt">
		<?php
			/* translators: %s: Name of current post */
			the_excerpt();
		?>
		</div>
	</div>
</article><!-- #post-## -->
