<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 */

get_header(); ?>
	<?php if ( have_posts() ) { ?>
		<div id="maincontent" class="grid-container">
			<div class="grid-x grid-margin-x">
				<?php while ( have_posts() ) : the_post(); ?>
				<div class="cell">
					<?php get_template_part( 'template-parts/page/content', 'front-page' ); ?>
				</div>
				<?php endwhile; // End of the loop. ?>
				<?php wp_reset_postdata();?>
			</div>	
		</div><!-- .maincontent -->
	<?php } else { ?>
			<?php get_template_part( 'template-parts/post/content', 'none' ); ?>
	<?php } ?>
<?php get_footer();
