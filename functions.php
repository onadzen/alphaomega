<?php
// we're firing all out initial functions at the start
add_action( 'after_setup_theme', 'init_theme', 16 );
function init_theme() {
    // launching operation cleanup
    add_action( 'init', 'head_cleanup' );
    //add custom menu
	add_action('init', 'register_custom_menu');
	// remove WP version from RSS
    add_filter( 'the_generator', 'rss_version' );
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action( 'wp_head', 'remove_recent_comments_style', 1 );
    // clean up gallery output in wp
    add_filter( 'gallery_style', 'gallery_style' );
    // enqueue base scripts and styles
    add_action( 'wp_enqueue_scripts', 'scripts_and_styles', 999 );
	add_theme_support( 'post-thumbnails' );	
	//add_post_type_support( 'post_type', 'woosidebars' );
} /* end  */
/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/
function head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 );
	// Remove Admin bar
	//add_filter('show_admin_bar', 'remove_admin_bar');
} /* end head cleanup */
// Remove Admin bar
//function remove_admin_bar()
//{
//    return false;
//}
// register menu locations
function register_custom_menu() {
	register_nav_menu('main-menu', __('Main Menu'));
	register_nav_menu('footer-menu', __('Footer Menu'));
}
// remove WP version from RSS
function rss_version() { return ''; }
// remove WP version from scripts
function remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
// remove injected CSS for recent comments widget
function remove_wp_widget_recent_comments_style() {
   if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
      remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
   }
}

//add logo function 
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 117,
        'width'       => 382,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

// remove injected CSS from recent comments widget
function remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
  }
}
// remove injected CSS from gallery
function gallery_style($css) {
  return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}
/*********************
SCRIPTS & ENQUEUEING
*********************/
// loading modernizr and jquery, and reply script
function scripts_and_styles() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {

	// register foundation stylesheet
    wp_register_style( 'mainstyle', get_stylesheet_directory_uri() . '/style.css', array(), '', 'all' );
    wp_register_style( 'foundation', get_stylesheet_directory_uri() . '/css/app.css', array(), '', 'all' );
	wp_register_style( 'fontawesome-solid', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css' , array(), '', 'all' );
    //register custom styles sheets
	wp_register_style( 'opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800', array(), '', 'all' );
	wp_register_style( 'custom-styles', get_stylesheet_directory_uri() . '/css/custom-style.css', array(), '', 'all' );
    

    // comment reply script for threaded comments
    //if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
	//	wp_enqueue_script( 'comment-reply' );
    //}

    //adding scripts file in the footer
    // Drop this in functions.php or your theme
	//if( !is_admin()){
	//	wp_deregister_script('jquery');
	//	wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.1', true );
	//}
    // enqueue styles and scripts
	wp_enqueue_style( 'foundation');
	wp_enqueue_style( 'fontawesome-solid');
	wp_enqueue_style( 'opensans');
	wp_enqueue_style( 'mainstyle');
    //wp_enqueue_script('jquery');
  	
    /*
    I recommend using a plugin to call jQuery
    using the google cdn. That way it stays cached
    and your site will load faster.
    */
  }
}
/************* ACTIVE WIDGETS ********************/
// Widgetizes Areas
function theme_widgets_init() {
	register_sidebar(array(
		'name'=>'Top Menu Widget',
		'id' => 'top-menu-widget',
		'description' => __( 'This is the top menu widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Request Quote Widget',
		'id' => 'request-quote-widget',
		'description' => __( 'This is the right request quote widget' ),
		'before_widget' => '<div id="%1$s" class="widget btn-request-quote">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Default Widget',
		'id' => 'widget-sidebar',
		'description' => __( 'This is the default sidebar widget' ),
		'before_widget' => '<div id="%1$s" class="widget sidbear-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Top Footer Widget',
		'id' => 'top-footer-widget',
		'description' => __( 'This is located above the footer widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Footer Widget First',
		'id' => 'footer-widget-first',
		'description' => __( 'This is the left footer widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Footer Widget Second',
		'id' => 'footer-widget-second',
		'description' => __( 'This is the left footer widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Footer Widget Third',
		'id' => 'footer-widget-third',
		'description' => __( 'This is the right footer widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Footer Widget Fourth',
		'id' => 'footer-widget-fourth',
		'description' => __( 'This is the right footer widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
}
// Register sidebars by running tutsplus_widgets_init() on the widgets_init hook.
add_action( 'widgets_init', 'theme_widgets_init' );

class F6_DRILL_MENU_WALKER extends Walker_Nav_Menu {
	/*
	 * Add vertical menu class
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul class=\"vertical menu\">\n";
	}
}

function f6_drill_menu_fallback( $args ) {
	/*
	 * Instantiate new Page Walker class instead of applying a filter to the
	 * "wp_page_menu" function in the event there are multiple active menus in theme.
	 */
	$walker_page = new Walker_Page();
	$fallback    = $walker_page->walk( get_pages(), 0 );
	$fallback    = str_replace( "children", "children vertical menu", $fallback );
	echo '<ul class="menu vertical nested">' . $fallback . '</ul>';
}

// remove LINKS menu item
add_action( 'admin_menu', 'my_admin_menu' );

function my_admin_menu() {
     remove_menu_page('link-manager.php');
}

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
	global $post;
 return '<p class="readmore"><a class="moretag" href="'. get_permalink($post->ID) . '"> Read more <i class="fas fa-angle-double-right"></i></a></p>';
}
add_filter('excerpt_more', 'new_excerpt_more');

//add data hover to menu items
add_filter( 'nav_menu_link_attributes', 'add_custom_data_atts_to_nav', 10, 4 );
function add_custom_data_atts_to_nav( $atts, $item, $args ) {

$atts['data-hover'] = $item->title;
return $atts;}

//hook the administrative header output
//add_action('admin_head', 'my_custom_logo');
//function my_custom_logo() {
//   echo '
//      <style type="text/css">
//         #header-logo { background-image: url('.get_bloginfo('template_directory').'/images/custom-logo.gif) !important; }
//      </style>
//   ';
//}

// Register Custom Post Type
function projects_post_type() {

	$labels = array(
		'name'                  => _x( 'Projects', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Projects', 'text_domain' ),
		'name_admin_bar'        => __( 'Project', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Project:', 'text_domain' ),
		'all_items'             => __( 'All Project', 'text_domain' ),
		'add_new_item'          => __( 'Add New Project', 'text_domain' ),
		'add_new'               => __( 'New Project', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Project', 'text_domain' ),
		'update_item'           => __( 'Update Project', 'text_domain' ),
		'view_item'             => __( 'View Project', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search projects', 'text_domain' ),
		'not_found'             => __( 'No projects found', 'text_domain' ),
		'not_found_in_trash'    => __( 'No projects found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Project', 'text_domain' ),
		'description'           => __( 'Project information pages.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'custom-fields' ),
		'taxonomies'            => array( '' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'project', $args );

}
add_action( 'init', 'projects_post_type', 0 );

/*function to add async and defer attributes*/
function defer_js_async($tag){

## 1: list of scripts to defer.
$scripts_to_defer = array();
## 2: list of scripts to async.
$scripts_to_async = array();
 
#defer scripts
foreach($scripts_to_defer as $defer_script){
	if(true == strpos($tag, $defer_script ) )
	return str_replace( ' src', ' defer="defer" src', $tag );	
}
#async scripts
foreach($scripts_to_async as $async_script){
	if(true == strpos($tag, $async_script ) )
	return str_replace( ' src', ' async="async" src', $tag );	
}
return $tag;
}
add_filter( 'script_loader_tag', 'defer_js_async', 10 );